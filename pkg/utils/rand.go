package utils

import (
	"math/rand"
	"time"
)

var randSource = rand.New(rand.NewSource(time.Now().UnixNano()))

// RandString 随机字符串
func RandString(count int) string {
	bytes := make([]byte, count)
	for i := 0; i < count; i++ {
		b := randSource.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return string(bytes)
}

// RandInt 随机范围中的数字
func RandInt(start int, end int) int {
	rand.Seed(time.Now().UnixNano())
	v := rand.Intn(end - start)
	v = start + v
	return v
}
