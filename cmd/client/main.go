package main

import (
	"context"
	"fmt"

	"gitee.com/lichenxin/grpc-consul-server/pkg/room"
)

func main() {
	c := room.GetClient()

	for i := 1; i <= 100; i++ {
		rs, err := c.Join(context.Background(), int64(i))
		if err != nil {
			panic(err)
		}
		fmt.Println(rs.RoomID, rs.MemberCount)
	}
}
