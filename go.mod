module gitee.com/lichenxin/grpc-consul-server

go 1.16

require (
	github.com/hashicorp/consul/api v1.8.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
