# 基于consul的grpc服务发现处理

### GRPC Quick start
```
https://grpc.io/docs/languages/go/quickstart/
```

### 安装consul
```
docker pull consul
docker run --name consul -d -p 8500:8500 consul
```

### 浏览器打开
```
 http://127.0.0.1:8500
```

### 编译服务运行
```
go build cmd/server/main.go 
```
